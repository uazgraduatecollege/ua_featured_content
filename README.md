# UA Featured Content* (Carousel) Drupal Feature Module

(*) Name subject to change.

Provides content type and view for Featured Content Carousel component consistent with UA brand strategy.

## Features

- Provides 'featured_content' content type.
- Provides carousel view for featured_content items.
- Provides an administrative view for re-ordering carousel items.
- Provides 'uaqs_hero_carousel' Flexslider preset.

## Packaged Dependencies

When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following dependencies will be automatically packaged with the distribution.

### Drupal Contrib Modules

- [Flexslider](https://www.drupal.org/project/flexslider)

### Libraries

- [Flexslider](http://www.woothemes.com/flexslider/)

## Other Dependencies
When this module is used outside of a Drupal distribution, the packaged dependencies above as well as these additional module dependencies will need to be installed manually.

- [Chaos tool suite (ctools)](https://www.drupal.org/project/ctools)
- [Features](https://www.drupal.org/project/features)
- [DraggableViews](https://www.drupal.org/project/draggableviews)
- [Entiy API](https://www.drupal.org/project/entity)
- [jQuery Update](https://www.drupal.org/project/jquery_update)
- [Link](https://www.drupal.org/project/link)
- [Strongarm](https://www.drupal.org/project/strongarm)
- [UAQS Fields](https://bitbucket.org/ua_drupal/uaqs_fields)

## Installation

After enabling this module, also make sure that `views_ui` is enabled and give users the following permisisons:

* Use Contextual Links
* Use Draggable Views

If using `external_links` module, make sure to add the CSS ID for this module to the list of exclusions.